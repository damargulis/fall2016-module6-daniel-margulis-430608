
var http = require("http"),
	socketio = require("socket.io"),
	fs = require("fs");

var app = http.createServer(function(req, resp){
	fs.readFile('client.html', function(err, data){
		if(err) return resp.writeHead(500);
		resp.writeHead(200, {
			"Content-Type": 'text/html'
		});
		resp.end(data);
	});
});
app.listen(3456);

//holds all chatrooms
var chatrooms = {
	'Main Lobby': {
		private: false,
		users: [],
		sockets: [],
		creator: null,
		banned: []
	},
};

//holds all users
var users = [];

var io = socketio.listen(app);
io.sockets.on("connection", function(socket){

	//public message sent
	socket.on('message_to_server', function(data) {
		io.to(data['chatroom']).emit('message_to_client', {
			message: data['message'],
			user: socket.username,
			color: data['color'],
		});
	});

	//private message sent
	socket.on('private-message', function(data) {
		var chatroom = chatrooms[data['chatroom']];
		var index = chatroom['users'].indexOf(data['recepient']);
		if(index > -1){
			var socket_to_receive = chatroom['sockets'][index];
			socket_to_receive.emit('receive-private-message', {
				message: data['message'],
				user: socket.username,
			});
		}

	})

	//public chatroom created
	socket.on('create-chatroom', function(data, callback) {
		console.log('createing' + data['name']);
		chatrooms[data['name']] = {
			private: false,
			users: [],
			sockets: [],
			creator: socket,
			banned: []
		};
		socket.broadcast.emit("chatroom-created", {name: data['name'], private:false});
		callback();
	});

	//private chatroom created
	socket.on('create-private-chatroom', function(data, callback) {
		chatrooms[data['name']] = {
			private: true,
			password: data['password'],
			users: [],
			sockets: [],
			creator: socket,
			banned: [],
		};
		socket.broadcast.emit("chatroom-created", {name: data['name'], private:true});
		callback();
	})

	//chatroom deleted
	socket.on('delete-room', function(data) {
		delete chatrooms[data['chatroom']];
		io.sockets.emit("chatroom-deleted", {name: data['chatroom']});
	})

	//new user logins in
	socket.on('login', function(data) {
		for(var i=0; i<users.length; i++){
			if(users[i] === data['username']){
				socket.emit("login-failed");
				return;
			}
		}
		socket.username = data['username'];
		users.push(data['username']);
		socket.emit("login-success", {username:data['username']});
	});

	//user joins new chatroom
	socket.on('join-chatroom', function(data) {
		//check if you can join
		for(var i=0; i<chatrooms[data['chatroom']].banned.length; i++){
			if(socket === chatrooms[data['chatroom']].banned[i]){
				socket.emit('cant-enter', {chatroom: data['chatroom']});
				return;
			}
		}
		//leave old chatroom
		socket.leave(socket.current_chatroom);
		io.to(socket.current_chatroom).emit('user-left-chatroom', {username: socket.username});
		if(chatrooms[socket.current_chatroom]){
			chatrooms[socket.current_chatroom]['users'].splice(chatrooms[socket.current_chatroom]['users'].indexOf(socket.username), 1);
			chatrooms[socket.current_chatroom]['sockets'].splice(chatrooms[socket.current_chatroom]['sockets'].indexOf(socket), 1);
		}
		//join jew chatroom
		var isOwner = (socket === chatrooms[data['chatroom']]['creator']);
		socket.emit('join-chatroom-success', {
			chatroom: data['chatroom'],
			users: chatrooms[data['chatroom']]['users'],
			creator: isOwner
		});
		//tell others about new user
		chatrooms[data['chatroom']]['users'].push(socket.username);
		chatrooms[data['chatroom']]['sockets'].push(socket);
		io.to(data['chatroom']).emit('user-joined-chatroom', {username: socket.username});
		socket.join(data['chatroom']);
		socket.current_chatroom = data['chatroom'];
	});

	//user joins private chatroom
	socket.on('join-private-chatroom', function(data) {
		var pwguess = data['password'];
		var pwactual = chatrooms[data['chatroom']].password;
		//check password
		if(pwguess === pwactual){
			//check for banned
			for(var i=0; i<chatrooms[data['chatroom']].banned.length; i++){
				if(socket === chatrooms[data['chatroom']].banned[i]){
					socket.emit('cant-enter', {chatroom: data['chatroom']});
					return;
				}
			}
			//leave old chatroom
			socket.leave(socket.current_chatroom);
			io.to(socket.current_chatroom).emit('user-left-chatroom', {username: socket.username});
			if(chatrooms[socket.current_chatroom]){
				chatrooms[socket.current_chatroom]['users'].splice(chatrooms[socket.current_chatroom]['users'].indexOf(socket.username), 1);
				chatrooms[socket.current_chatroom]['sockets'].splice(chatrooms[socket.current_chatroom]['users'].indexOf(socket), 1);
			}
			//join new chatroom
			var isOwner = (socket === chatrooms[data['chatroom']]['creator']);
			socket.emit('join-chatroom-success', {
				chatroom: data['chatroom'],
				users: chatrooms[data['chatroom']]['users'],
				creator: isOwner
			});

			//tell users about new join
			chatrooms[data['chatroom']]['users'].push(socket.username);
			chatrooms[data['chatroom']]['sockets'].push(socket);
			io.to(data['chatroom']).emit('user-joined-chatroom', {username: socket.username});
			socket.join(data['chatroom']);
			socket.current_chatroom = data['chatroom'];
		}else{
			socket.emit('private-join-failure');
		}
	});

	//kick out specific user
	socket.on('kick-user', function(data) {
		var chatroom = chatrooms[data['chatroom']];
		if(chatroom.creator !== socket){
			console.log("INTRUDER!");
			return;
		};
		var index = chatroom['users'].indexOf(data['user']);
		if(index > -1){
			var socket_to_kick = chatroom['sockets'][index];
			socket_to_kick.emit('youve-been-kicked', {chatroom: data['chatroom']});
		}

	});

	//ban specifc user
	socket.on('ban-user', function(data) {
		var chatroom = chatrooms[data['chatroom']];
		if(chatroom.creator !== socket){
			console.log("INTRUDER!");
			return;
		}
		var index = chatroom['users'].indexOf(data['user']);
		if(index > -1){
			var socket_to_ban = chatroom['sockets'][index];
			socket_to_ban.emit('youve-been-banned', {chatroom: data['chatroom']});
			chatroom.banned.push(socket_to_ban);
		}
	})

	//on disconnect, user leaves chatroom and global user list
	socket.on('disconnect', function(data) {
		io.to(socket.current_chatroom).emit('user-left-chatroom', {username: socket.username});
		if(chatrooms[socket.current_chatroom]){
			var index = chatrooms[socket.current_chatroom]['users'].indexOf(socket.username)
			if(index > -1){
				chatrooms[socket.current_chatroom]['users'].splice(index, 1);
				chatrooms[socket.current_chatroom]['sockets'].splice(index, 1);
			}
		}
		var userIndex = users.indexOf(socket.username);
		if(userIndex > -1){
			users.splice(userIndex, 1);
		}
	})

	//initial load, push all current chatrooms
	for(chatroom in chatrooms){
		socket.emit('chatroom-created', {name: chatroom, private: chatrooms[chatroom].private});
	};

});
