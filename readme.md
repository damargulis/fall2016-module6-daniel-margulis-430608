[link](http://ec2-54-201-84-1.us-west-2.compute.amazonaws.com:3456)

Login with any username.  You will be placed into the main lobby by deault.  New rooms can be created using the form on the left.  Leaving the password field blank will create a public room, or a password can be used to create a private one.  Users can be private messaged by clicking the message button next to their name.  Chatrooms can be joined using the list on the left.  

For the creative portion, I added the ability to delete chatrooms if you were the one to create them.  Anyone who is still in the chatroom when it is delted gets alerted and kicked out.

I also enabled the user to selected a color to send their message in.  Using the dropdown menu, they can pick a color and the message will be sent to all other uses using that color.
